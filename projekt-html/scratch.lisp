
(load "/home/jack/quicklisp/setup.lisp")
(push :hunchentoot-no-ssl *features*)
(ql:quickload '(:swank :cl-who :hunchentoot :parenscript :elephant))

(defpackage :md-app
  (:use :cl :cl-who :hunchentoot :parenscript :elephant))
(in-package :md-app)

(defparameter *path* 
  (make-pathname :directory '(:absolute "home" "jack" "lisp_server")
		 :type "lisp"))

;;; Zawiera definicje języka dziedzinowego
;; generatory html
(load (merge-pathnames *path* "html-dsl"))
;; klasy specyficzne dla aplikacji
(load (merge-pathnames *path* "users"))
(load (merge-pathnames *path* "center"))
(load (merge-pathnames *path* "data-def"))
(load (merge-pathnames *path* "calendar"))

;; podstrony
(load (merge-pathnames *path* "scripts"))
(load (merge-pathnames *path* "page-def"))
(load (merge-pathnames *path* "forms.lisp"))

;;; Start serwera
(load (merge-pathnames *path* "server.lisp"))

