
(in-package :md-app)

(defpclass przychodnia ()
  ((nazwa :reader nazwa
	  :initarg :nazwa
	  :index t)))

(defun znajdz-przychodnie (nazwa)
  (get-instances-by-value 'przychodnia 'nazwa nazwa))

(defun dodaj-przychodnie (nazwa)
  (with-transaction ()
    (unless (znajdz-przychodnie nazwa)
      (make-instance 'przychodnia :nazwa nazwa))))
