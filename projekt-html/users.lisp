
(in-package :md-app)

;; klasa użytkownika
(defpclass uzytkownik ()
  ((ident :reader ident
	  :initform (elephant-data-store:next-oid *store*)
	  :index t)
   (login :reader login
	  :initarg :login
	  :index t)
   (haslo :reader haslo
	  :initarg :haslo)
   (status :reader status
	   :initarg :status
	   :index t)
   (dane :accessor dane
	 :initarg :dane)
   (aktywny :accessor aktywny
	    :initform nil
	    :index t)))

(defun loguj (login haslo)
  (let ((user (get-instance-by-value 'uzytkownik 'login login)))
    (if user
	(and (aktywny user)
	     (equalp (md5:md5sum-string haslo) (haslo user))
	     (setf (session-value :status) user)
	     T))))

(defun znajdz-uzytkownika (login)
  (get-instance-by-value 'uzytkownik 'login login))

(defun znajdz-po-id (id)
  (get-instance-by-value 'uzytkownik 'ident id))

(defun dodaj-uzytkownika (login haslo dane)
  (with-transaction ()
    (unless (znajdz-uzytkownika login)
      (format t "dodaje ~a:~a" login haslo)
      (make-instance 'uzytkownik 
		     :login login
		     :haslo (md5:md5sum-string haslo)
		     :status (type-of dane)
		     :dane dane))))

(defun aktywuj-uzytkownika (login)
  (setf (aktywny (znajdz-uzytkownika login)) T))

(defun usun-uzytkownika (login)
  (drop-instance (znajdz-uzytkownika login)))

(defun listing (klasa &optional typ wartosc)
  (remove-duplicates
   (if (and typ wartosc)
       (get-instances-by-value klasa typ wartosc)
       (get-instances-by-class klasa))))
