
(in-package :md-app)

(defpclass osoba (uzytkownik)
  ((imie :accessor imie
	 :initarg :imie)
   (nazwisko :accessor nazwisko
	     :initarg :nazwisko)))

(defpclass administrator (osoba) ())

(defpclass lekarz (osoba)
  ((pwz :reader pwz
	:initarg :pwz)))

(defpclass pacjent (uzytkownik)
  ((pesel :accessor pesel
	  :initarg :pesel)))

(defun nowy-lekarz (imie nazwisko pwz)
  (make-instance 'lekarz 
		 :imie imie
		 :nazwisko nazwisko
		 :pwz pwz))

(defun nowy-pacjent (imie nazwisko pesel)
  (make-instance 'pacjent
		 :imie imie
		 :nazwisko nazwisko
		 :pesel pesel))

(defun nowy-administrator (imie nazwisko)
  (make-instance 'administrator
		 :imie imie
		 :nazwisko nazwisko))

(defun przedstaw (osoba)
  (stringify (imie osoba) " " (nazwisko osoba)))

;(defmethod zaklep ((lek lekarz)
;		   (por poradnia)
;		   czas-startu
;		   czas-konca)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (defun dodaj-przychodnie (nazwa)			       ;;
;;   (unless (znajdz-przychodnie nazwa)			       ;;
;;     (push (make-instance 'przychodnia :nazwa nazwa) 	       ;;
;; 	  *przychodnie*)))				       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun inicjalizuj ()
  (drop-instances (append 
		   (listing 'uzytkownik)
		   (listing 'lekarz)
		   (listing 'pacjent)
		   (listing 'administrator)
		   (listing 'przychodnia)))

  (mapcar 'dodaj-uzytkownika
	  (list "lek1" "lek2" "lek3" "pac1" "pac2" "pac3" "adm1" "adm2" "adm3" "por1" "por2" "por3" "por4")
	  (list "kotk" "kotk" "kotk" "kotk" "kotk" "kotk" "kotk" "adm2" "adm3" "kotk" "kotk" "kotk" "kotk")
	  (append
	   (mapcar (lambda (imie nazwisko pwz)
		     (make-instance 'lekarz
				    :imie imie
				    :nazwisko nazwisko
				    :pwz pwz))
		   (list "Andrzej" "Maria" "Kleofasa")
		   (list "Koterski" "Kutwa" "Izdabelińska")
		   (list "12345" "23456" "34567"))

	   (mapcar (lambda (imie nazwisko pesel)
		     (make-instance 'pacjent
				    :imie imie
				    :nazwisko nazwisko
				    :pesel pesel))
		   (list "Matylda" "Barbara" "Karolina")
		   (list "Górczyńska" "Basta" "Bęcwał")
		   (list "12345" "23456" "34567"))

	   (mapcar (lambda (imie nazwisko)
		     (make-instance 'administrator
				    :imie imie
				    :nazwisko nazwisko))
		   (list "Ferdynand" "Kandela" "Grzegorz")
		   (list "Kiepści" "Cukierek" "Muszyński"))

	   (mapcar (lambda (nazwa)
		     (make-instance 'przychodnia
				    :nazwa nazwa))
		   (list "Przychodnia im. Andrzeja Skwarka w Szczebrzeszynie"
			 "Przychodnia kameralna sp. z.o.o. na ul. Błyszkotliwej"
			 "Różowa panterka - przychodnia wterynaryjna"
			 "Jasnogórska przychodnia duszpasterska w Zielonej Górze"))))
    
  (mapcar 'aktywuj-uzytkownika (mapcar 'login (listing 'uzytkownik))))
