
(in-package :md-app)

(defpclass wizyta ()
  ((lekarz :accessor lekarz
	   :initarg :lekarz
	   :index t)
   (pacjent :accessor pacjent
	    :initarg :pacjent
	    :index t)
   (dyzur :accessor dyzur
	  :initarg :dyzur
	  :index t)
   (czas-wizyty :accessor czas-wizyty
		:initarg :czas-wizyty
		:index t)
   (rezerwacja :reader rezerwacja
	       :initform (get-universal-time))
   (potwierdzona :accessor potwierdzona
		 :initform nil
		 :index t)))

(defpclass dyzur ()
  ((lekarz :accessor lekarz
	   :initarg :lekarz
	   :index t)
   (przychodnia :accessor przychodnia
		:initarg :przychodnia
		:index t)
   (dzien :accessor dzien
	  :initarg :dzien
	  :index t)
   (godzina :accessor godzina
	    :initarg :godzina
	    :index t)))

(defun dyzury (uzytkownik &optional day hour)
  (let ((usr-dyz (get-instances-by-value 'dyzur (type-of uzytkownik) uzytkownik)))
    (if (and day hour)
        (intersection
	 (intersection
	  usr-dyz
	  (get-instances-by-value 'dyzur 'dzien day))
	 (get-instances-by-value 'dyzur 'godzina hour))
	usr-dyz)))

(defparameter *dni-tygodnia*
  (list "Poniedziałek" "Wtorek" "Środa" "Czwartek" "Piątek" "Sobota" "Niedziela"))

(defparameter *miesiące*
  (list "Stycznia" "Lutego" "Marca" "Kwietnia" "Maja" "Czerwca" 
	"Lipca" "Sierpnia" "Września" "Października" "Listopada" "Grudnia"))

(defparameter *godziny*
  (mapcar (lambda (h)
	    (format nil "~2,'0d:~2,'0d"
		    (truncate (+ (/ h 2) 8))
		    (* 30 (rem h 2))))
	  (reverse 
	   (do ((i 0 (1+ i))
		(l nil (cons i l)))
	       ((= i 25) l)))))

(defmethod wolne-terminy (dr &optional (czas (get-universal-time)))
  (let ((wz (sort (intersection (get-instances-by-value 'wizyta 'dyzur dr)
				(get-instances-by-range 'wizyta 'czas-wizyty czas (+ czas 2678400)))
		  (lambda (x y) (< (czas-wizyty x) (czas-wizyty y))))))
    (do ((start-day 
	  (multiple-value-bind (sek min hrs day mth yrs wk)
	      (decode-universal-time czas)
	    (declare (ignore sek min))
	    (let ((m (* (mod (godzina dr) 2) 30))
		  (h (truncate (+ (/ (godzina dr) 2) 8)))
		  (w (dzien dr)))
	      (+ (encode-universal-time 0 m h day mth yrs)
		 (* 86400 ; 86400 = (* 60 60 24) = dzień
		    (cond
		      ((< w wk) (+ 7 (- w wk)))
		      ((> w wk) (- w wk))
		      ((< h hrs) 7)
		      (T 0))))))
	  (+ start-day 604800))   ; 604800 = (* 60 60 24 7) = tydzień
	 (acc nil))
	((= (length acc) 3) acc)
	(if (or (not wz)
		(not (= (czas-wizyty (car wz)) start-day)))
	    (push start-day acc)
	    (setf wz (cdr wz))))))

(defun utc-to-string (utc)
  (multiple-value-bind (sek min hrs day mth yrs)
      (decode-universal-time utc)
    (declare (ignore sek min hrs))
    (format nil "~a ~a ~a" day (nth (1- mth) *miesiące*) yrs)))

(defun day-of-week (utc)
  "Poniedziałek to 0"
  (nth-value 6 (decode-universal-time utc)))

(defun hour-of-day (utc)
  (let ((h (nth-value 2 (decode-universal-time utc)))
	(m (nth-value 1 (decode-universal-time utc))))
    (+ (* (- h 8) 2) (nth-value 0 (floor (/ m 30))))))

(defmethod umowione (osoba)
  (do ((wiz (get-instances-by-value 'wizyta (type-of osoba) osoba) (cdr wiz))
       (res (make-array '(7 24) :initial-element nil)))
      ((not wiz) res)
    (push (car wiz)
	  (aref res 
		(day-of-week (czas-wizyty (car wiz)))
		(hour-of-day (czas-wizyty (car wiz)))))))

(defmethod umowiona? (osoba d c)
  (do ((wiz (get-instances-by-value 'wizyta (type-of osoba) osoba) (cdr wiz))
       (res nil 
  	    (and (eql d (day-of-week (czas-wizyty (car wiz))))
  		 (eql c (hour-of-day (czas-wizyty (car wiz)))))))
      ((or (not wiz) res) res)))
  
(defmethod potwierdzona? ((wizyta wizyta))
  (wizyta-potwierdzona wizyta))

  ;;   ((> (- (wizyta-czas-rezerwacji wizyta)
  ;; 	   (get-universal-time))
  ;; 	(* 10 60)) 
     

  ;; (dolist (dzien 
  ;; 	    (list "Poniedziałek"
  ;; 		  "Wtorek" 
  ;; 		  "Środa" 
  ;; 		  "Czwartek"
  ;; 		  "Piątek"
  ;; 		  "Sobota"
  ;; 		  "Niedziela
