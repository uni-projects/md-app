
(defparameter *formularz-logowania*
  (list (list "Login" "login" "text")
	(list "Hasło" "haslo" "password")))

(defparameter *formularz-rejestracji*
  (list (list "Imię" "imie" "text")
	(list "Nazwisko" "nazwisko" "text")
	(list "Pesel" "pesel" "number")
	(list "Login" "login" "text")
	(list "Hasło" "haslo" "password")))

(defparameter *formularz-dodawania-lekarza*
  (list
   (list "Imię" "imie" "text")
   (list "Nazwisko" "nazwisko" "text")
   (list "Numer PWZ" "pwz" "number")
   (list "Login" "login" "text")
   (list "Hasło" "haslo" "password")))

(defparameter *formularz-dodawania-poradni*
  (list
   (list "Nazwa przychodni" "nazwa" "text")))

(defparameter *rozklad-naglowek*
  (list ""
	"Poniedziałek" "Wtorek" "Środa" "Czwartek" "Piątek" "Sobota" "Niedziela"))

(defparameter *greetings* (make-hash-table :test #'equalp))
(setf (gethash 'administrator *greetings*)
      (with-html-output-to-string
	  (*standard-output* nil :indent t)
	(:span :class "greet" "Jesteś zalogowany jako" (:i "Administrator"))
	(:div :id "nav"
	      (:a :href "/" "Start")
	      (:a :href "wizyty" "Wizyty")
	      (:a :href "poradnie" "Poradnie")
	      (:a :href "lekarze" "Lekarze")
	      (:a :href "pacjenci" "Pacjenci")
	      (:a :href "wyloguj" "Wyloguj"))))

(setf (gethash 'pacjent *greetings*)
      (with-html-output-to-string
	  (*standard-output* nil :indent t)
	(:span :class "greet" "Jesteś zalogowany jako" (:i "Pacjent"))
	(:div :id "nav"
	      (:a :href "/" "Start")
	      (:a :href "wizyty" "Twoje wizyty")
	      (:a :href "umow" "Umów wizytę")
	      (:a :href "ustawienia" "Ustawienia")
	      (:a :href "wyloguj" "Wyloguj"))))

(setf (gethash 'lekarz *greetings*)
      (with-html-output-to-string
	  (*standard-output* nil :indent t)
	(:span :class "greet" "Jesteś zalogowany jako" (:i "Lekarz"))
	(:div :id "nav"
	      (:a :href "/" "Start")
	      (:a :href "/wizyty" "Lista wizyt")
	      (:a :href "/rozklad" "Plan pracy")
	      (:a :href "/ustawienia" "Ustawienia")
	      (:a :href "/wyloguj" "Wyloguj"))))

(setf (gethash 'guest *greetings*)
      (with-html-output-to-string
	  (*standard-output* nil :indent t)
	(:div :id "greet" "Jesteś zalogowany jako" (:i "Gość"))
	(:div :id "nav"
	      (:a :href "/" "Start")
	      (:a :href "poradnie" "Poradnie")
	      (:a :href "lekarze" "Lekarze")
	      (:a :href "zaloguj" "Zaloguj")
	      (:a :href "rejestracja" "Zarejestruj"))))
