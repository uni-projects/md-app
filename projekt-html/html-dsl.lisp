(in-package :md-app)

(setf (html-mode) :html5)

(defmacro standard-page ((&key title privigiled) &body body)
  "Makro zawiera szablon strony"
  `(start-session)
  `(let ((status 'guest))
     (if (session-value :status)
	 (setf status (class-name (class-of (dane (session-value :status))))))
     (unless (or (find status ,privigiled) (not ,privigiled))
       (handle-static-file "errors/404.html"))

     (with-html-output-to-string 
	 (*standard-output* nil :prologue t :indent t)
       (:html
	(:head (:meta :charset "utf-8")
	       (:title ,title)
	       (:link :type "text/css"
		      :rel "stylesheet"
		      :href "/md-app.css")
	       (:script :type "text/javascript"
			:src "/scripts/forms.js"))
		      
	(:body
	 (:div :id "header" ; logo, cokolwiek
	       (format t "~a" (gethash status *greetings*)))
	 (:hr)
	,@body
	(:hr)
	(:div :id "footer" ; stopka
	      (:img :src "/img/made-with-lisp-logo.jpg"
		    :width 300 :height 100)))))))

(defmacro standard-plan ((funkcja))
  `(htm
    (:table 
     :id "rozklad"
     (:tr (:th)
	  (dolist (th *dni-tygodnia*)
	    (htm (:th (fmt "~a" th)))))
     (loop for hour from 0 to 23 do
	  (htm (:tr (:th (fmt (nth hour *godziny*)))
		    (loop for day from 0 to 6 do
			 (funcall ,funkcja day hour))))))))


(defmacro standard-form ((label dest inputs) &body body)
  `(htm
    (:label ,label)
    (:form :action ,dest :method "post"
	   (:table
	    (dolist (tup ,inputs)
	      (htm
	       (:tr
		(:td (:label (fmt "~a" (car tup))))
		(:td (:input :name (cadr tup) :type (caddr tup))))))
	    ,@body
	    (:tr
	     (:td :colspan 2 
		  (:input :type "submit" 
			  :value "Prześlij formularz")))))))

(defmacro sorted-dyzur (&body body)
  `(sort
    ,@body
    (lambda (x y) (if (= (godzina x) (godzina y))
		      (< (dzien x) (dzien y))
		      (< (godzina x) (godzina y))))))

(defmacro loop-dyzur ((v dr d h) A)
  `(do ((,v (car ,dr) (car ,dr)))
       ((not (and ,v
		  (= ,d (dzien ,v))
		  (= ,h (godzina ,v)))))
     (prog1 ,A
       (setf ,dr (cdr ,dr)))))
	
(defmacro if-dyzur ((dr d h) A B)
  `(if (and ,dr
	    (= ,d (dzien (car ,dr)))
	    (= ,h (godzina (car ,dr))))
       (progn ,A)
       (progn ,B)))

(defmacro combobox ((name elements indexes &optional (sel -1)))      
  `(htm
    (:select :name ,name
	     (do ((el ,elements (cdr el))
		  (id ,indexes (cdr id)))
		 ((not el))
	       (if (= ,sel (car id))
		   (htm (:option :value (car id) :selected "selected" (fmt "~a" (car el))))
		   (htm (:option :value (car id) (fmt "~a" (car el)))))))))


(defmacro def-validate-function (&body body)
  `(list 
    (ps (lambda ,@body))
    (lambda ,@body)))
