
(in-package :md-app)

(defparameter *httpd-port* 8080)
(defparameter *shutdown-port* 6200)
(defparameter *swank-port* 4005)

;; backend danych aplikacji
;; (defparameter *db-config*
;;   '(:clsql (:sqlite3 "/home/jack/public_html/md-app.db")))

;; (defparameter *db-config*
;;   '(:BDB "/home/jack/public_html/md-app-db/"))

(defparameter *db-config*
  '(:clsql (:postgresql 
	    "localhost" "md-app" "md-app" "md-app-password")))

(defparameter *httpd-/* "/home/jack/public_html/")
(defparameter *httpd-error* "/home/jack/public_html/errors/")

;; serwer lispa
(defparameter *swank-server*
  (swank:create-server :port *swank-port*
		       :dont-close t))

;; baza danych
(defparameter *store*
  (open-store *db-config*))

;; server www
(defparameter *httpd*
  (hunchentoot:start
   (make-instance 'hunchentoot:easy-acceptor 
		  :port *httpd-port*
		  :document-root *httpd-/*
		  :error-template-directory *httpd-error*)))

