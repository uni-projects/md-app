

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Operacje użytkowników ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-easy-handler (index :uri "/") ()
  "Główna strona portalu"
  (standard-page (:title "Strona główna")
    (:h1 "Witaj na stronie serwisu MD-APP")
    (cond
      ((eq status 'pacjent)
       (htm (:p "Chcesz umówić się na wizytę?"
		(:a :href "/wyszukaj" "Wyszukaj poradnię")
		"i zapisz się na wizytę do wybranego lekarza.")))
      ((eq status 'lekarz)
       (htm (:p "Możesz wybrać interesującą Cię akcję z menu nawigacyjnego.")))
      ((eq status 'administrator)
       (htm (:p "Great power comes with great responsibility")))
      ( T
       (htm
	(:p "Żeby umówić się na wizytę, musisz być"
	    (:a :href "/zaloguj" "zalogowany.")
	    (:br) "Nie masz konta?"
	    (:a :href "/rejestracja" "Zarejestruj się.")))))))

(define-easy-handler (zaloguj :uri "/zaloguj")
    ((login :request-type :POST)
     (haslo :request-type :POST))
  (start-session)
  (let ((zalogowany (loguj login haslo)))
    (standard-page (:title "Logowanie")
      (:h1 "Logowanie")
      (if (and login haslo)
	  (if zalogowany
	      (htm (:p "Zostałeś zalogowany pomyślnie."))
	      (htm (:p :class "error" "Logowanie nie powiodło się.")
		   (:a :href "zaloguj" "Spróbuj ponownie.")))
	  (standard-form ("Formularz logowania"
			 "zaloguj"
			 *formularz-logowania*))))))


(define-easy-handler (wyloguj :uri "/wyloguj") ()
  (delete-session-value :status)
  (if *SESSION* (remove-session *SESSION*))
  (standard-page (:title "Zostałeś wylogowany")
    (:h1 "Wylogowanie")
    (:p "Zostałeś wylogowany.")))

(define-easy-handler (rejestracja :uri "/rejestracja" :default-request-type :POST) 
    (imie nazwisko pesel login haslo)
  (standard-page (:title "Zarejestruj się"
			 :privigiled '(guest))
    (:h1 "Rejestracja")
    (if (and imie nazwisko pesel login haslo)
	(if (dodaj-uzytkownika login haslo (nowy-pacjent imie nazwisko pesel))
	    (htm
	     (:p "Rejestracja zakończona pomyślnie.")
	     (:a :href "zaloguj" "Możesz się zalogować."))
	    (htm (:p :class "error" "Rejestracja nie powiodła się.")
		 (:a :href "rejestracja" "Spróbuj ponownie.")))
	(standard-form ("Formularz rejestracyjny" 
			"rejestracja"
			*formularz-rejestracji*)))))

;;;;;;;;;;;;;;;;;;;;;;
;; Operacje lekarza ;;
;;;;;;;;;;;;;;;;;;;;;;
(define-easy-handler (rozklad :uri "/rozklad") ()
  (standard-page (:title "Twój plan pracy" :privigiled '(lekarz))
    (let* ((dr (sorted-dyzur (dyzury (dane (session-value :status)))))
	   (funkcja (lambda (day hour)
		      (if-dyzur (dr day hour)
				(prog1
				  (htm (:td :class "zajete" 
					    :onclick (format nil "delCell(~a, ~a)" day hour)
					    (fmt (nazwa (przychodnia (car dr))))))
				  (setf dr (cdr dr)))
				(prog1 
				  (htm (:td :class "wolne" 
					    :onclick (format nil "addCell(~a, ~a)" day hour)
					    (:img :class "t_op" :src "img/add.png"))))))))
      (standard-plan (funkcja)))))

(define-easy-handler (add :uri "/add")
    ((dzien-tygodnia :request-type :GET)
     (godzina :request-type :GET))
  (let ((hour (or (parse-integer (string godzina) :junk-allowed 0) 0))
	(day (or (parse-integer (string dzien-tygodnia) :junk-allowed 0) 0))
	(usr (listing 'uzytkownik 'status 'przychodnia)))
    (standard-page (:title "test" :privigiled '(lekarz))
      (standard-form ("Ustal godziny pracy" "/add-plan" nil)
	(:tr (:td "Przychodnia")
	     (:td (combobox ("przychodnia"
			     (mapcar (lambda (x) (nazwa (dane x))) usr)
			     (mapcar 'ident usr)))))
	(:tr (:td "Dzień tygodnia")
	     (:td (combobox ("dzien-tygodnia" 
			     *dni-tygodnia* 
			     (loop for i from 0 to 6 collecting i)
			     day))))
	(:tr (:td :style "vertical-align: top" "Czas pracy")
	     (:td  (combobox ("start" 
			      *godziny* 
			      (loop for i from 0 to (length *godziny*) collecting i)
			      hour))
		   (combobox ("stop" 
			      *godziny* 
			      (loop for i from 0 to (length *godziny*) collecting i)
			      (1+ hour) ))))))))

;;;;;;;;;;;;;;;;;;;;;;;
;; Operacje pacjenta ;;
;;;;;;;;;;;;;;;;;;;;;;;
(define-easy-handler (umow :uri "/umow") ()
  (standard-page (:title "Umów się na wizytę" :privigiled (list 'pacjent))
    (:h1 "Wybierz interesującą Cię przychodnię i lekarza")
    (let ((usr (listing 'uzytkownik 'status 'przychodnia))
	  (doc (listing 'uzytkownik 'status 'lekarz)))
      (standard-form ("" "/siatka" nil)
	(:tr (:td "Przychodnia")
	     (:td (combobox ("przychodnia"
			     (mapcar (lambda (x) (nazwa (dane x))) usr)
			     (mapcar 'ident usr)))))
	(:tr (:td "Lekarz")
	     (:td (combobox ("lekarz"
			     (mapcar (lambda (x) (przedstaw (dane x))) doc)
			     (mapcar 'ident doc)))))))))

(define-easy-handler (siatka :uri "/siatka")
    ((przychodnia :request-type :POST)
     (lekarz :request-type :POST))
  (standard-page (:title "Wybierz blok" :privigiled '(pacjent))
    (if (not przychodnia) (redirect "/errors/op"))
    (let* ((dr (sorted-dyzur
		 (if lekarz
		     (intersection (dyzury (dane (znajdz-po-id (parse-integer lekarz))))
				   (dyzury (dane (znajdz-po-id (parse-integer przychodnia)))))
		     (dyzury (dane (znajdz-po-id (parse-integer przychodnia)))))))
	   (funkcja (lambda (day hour)
		      (if-dyzur 
		       (dr day hour)
		       (htm (:td 
			     :class "zajete"
			     (:table 
			      :id "terminy"
			      (loop-dyzur (v dr day hour)
				 (htm 
				  (:tr 
				   (:th (fmt (przedstaw (lekarz v))))
				   (let ((wz (wolne-terminy v)))
				     (dolist (w (reverse wz))
				       (htm 
					(:tr 
					 (:td 
					  :onclick (format nil "subscribe(~a,~a,~a)"
							   w przychodnia lekarz)
					  (fmt (utc-to-string w)))))))))))))
		       (htm (:td :class "wolne"))))))
      (standard-plan
       (funkcja)))))

(define-easy-handler (ask :uri "/ask")
    ((dzien :request-type :GET)
     (godzina :request-type :GET)
     (lekarz :request-type :GET))
  (if (not (and dzien godzina lekarz)) (redirect "/errors/op"))
  (let ((day (parse-integer dzien))
	(hrs (parse-integer godzina))
	(lek (dane (znajdz-po-id (parse-integer lekarz)))))
    (standard-page (:title "Wybierz termin" :privigiled '(pacjent))
      (:h1 "Wybierz interesujący Cię termin")
      (:h2 (fmt "~a, godzina ~a (dr ~a)"
		(nth day *dni-tygodnia*)
		(nth hrs *godziny*)
		(przedstaw lek))))))

    
;;;;;;;;;;;;;;
;; Listingi ;;
;;;;;;;;;;;;;;
(define-easy-handler (poradnie :uri "/poradnie") ()
  (standard-page (:title "Wyszukaj poradnię")
    (:h1 "Poradnie dostępne w katalogu")
    (:div :id "przychodnie"
	  (:ul
	  (dolist (przychodnia (listing 'przychodnia))
	    (htm
	     (:li (fmt "~a" (nazwa przychodnia)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (define-easy-handler (lekarze :uri "/lekarze") ()		      ;;
;;       (standard-page (:title "Zarejestrowani lekarze")	      ;;
;; 	(:h1 "Zarejestrowani lekarze")				      ;;
;; 	(:div :class "lekarze"					      ;;
;; 	      (:table						      ;;
;; 	       (dolist (uzytkownik (listing 'lekarz))		      ;;
;; 		 (htm						      ;;
;; 		  (:tr						      ;;
;; 		   (:td (fmt "~a" (imie uzytkownik)))		      ;;
;; 		   (:td (fmt "~a" (nazwisko uzytkownik)))	      ;;
;; 		   (:td (fmt "~a" (pwz uzytkownik))))))))))	      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (define-easy-handler (pacjenci :uri "/pacjenci") () ;;
;;   (standard-page (:title "Zarejestrowani pacjenci") ;;
;;     (:h1 "Zarejestrowani pacjenci")		       ;;
;;     (:div :class "lekarze")))		       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (define-easy-handler (uzytkownicy :uri "/uzytkownicy") () ;;
;;   (standard-page (:title "Użytkownicy systemu")	     ;;
;;     (:h1 "Użytkownicy systemu md-app")))		     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

