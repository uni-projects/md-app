(in-package :md-app)

(define-easy-handler (user-scripts :uri "/scripts/user.js") ()
  (setf (content-type*) "text/javascript")
  (ps (defun delete-user (login)
	(setf accepted (confirm "Czy napewno chcesz usunąć użytkownika??"))
	(if accepted
	    (setf document.location.href (+ "/cc?usun=" login))))
      (defun lock-user (login)
	(setf document.location.href (+ "/cc?zablokuj=" login)))
      (defun unlock-user (login)
	(setf document.location.href (+ "/cc?odblokuj=" login)))))

(define-easy-handler (cc :uri "/cc")
    ((zablokuj :request-type :get)
     (odblokuj :request-type :get)
     (usun     :request-type :get))
  (standard-page (:title "akcja" :privigiled '(admin))
    (cond
      (usun 
       (usun-uzytkownika usun))
      (odblokuj
       (aktywuj-uzytkownika odblokuj))
      (zablokuj
       (aktywuj-uzytkownika zablokuj nil)))
    (redirect "/admin-panel")))


(define-easy-handler (add :uri "/admin-panel") ()
  (standard-page (:title "Panel administratora" :privigiled '(admin))
    (:h1 "Panel administratora")
    (:li (:a :href "dodaj-lekarza" "Dodaj lekarza"))
    (:li (:a :href "dodaj-poradnie" "Dodaj poradnię"))
    (:h2 "Pacjenci")
    (standard-listing (list "Imię" "Nazwisko" "Login" "Stan aktywacji" "Operacje")
      (do-lst (x 'pacjent) (htm (:tr (person-to-td x)))))
    (:h2 "Lekarze")
    (standard-listing (list "Imię" "Nazwisko" "Login" "Stan aktywacji" "Operacje")
      (do-lst (x 'lekarz) (htm (:tr (person-to-td x)))))
    (:h2 "Poradnie")
    (standard-listing (list "Nazwa" "Login" "Stan aktywacji" "Operacje")
      (do-lst (x 'poradnia) 
    	(htm (:tr
    	      (:td (fmt "~a" (nazwa x)))
	      (user-to-td x)))))
	      
    (:h2 "Wizyty")))

	 ;; (:tr
	 ;;  (:td (fmt "~a" (imie x)))
	 ;;  (:td (fmt "~a" (nazwisko x)))
	 ;;  (:td (fmt "~a" (pesel x)))
	 ;;  (:td (fmt "~a" (login x)))
	 ;;  (:td (if (aktywny x)
	 ;; 	   (htm
	 ;; 	    "aktywny: TAK"
	 ;; 	    (:a
	 ;; 	     :href (format nil "/pacjenci?zablokuj=~a" (login x))
	 ;; 	     "(zablokuj)"))
	 ;; 	   (htm 
	 ;; 	    "aktywny: NIE"
	 ;; 	    (:a
	 ;; 	     :href (format nil "/pacjenci?odblokuj=~a" (login x))
	 ;; 	     "(odblokuj)"))))))))))
