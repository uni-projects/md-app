(in-package :md-app)

(defmacro standard-plan ((funkcja))
  `(htm
    (:table 
     :id "rozklad"
     (:tr (:th)
	  (dolist (th *dni-tygodnia*)
	    (htm (:th (fmt "~a" th)))))
     (loop for hour from 0 to 23 do
	  (htm (:tr (:th (fmt (nth hour *godziny*)))
		    (loop for day from 0 to 6 do
			 (funcall ,funkcja day hour))))))))

(defmacro sorted-dyzur (&body body)
  `(sort
    ,@body
    (lambda (x y) (if (= (godzina x) (godzina y))
		      (< (dzien x) (dzien y))
		      (< (godzina x) (godzina y))))))

(defmacro loop-dyzur ((v dr d h) A)
  `(do ((,v (car ,dr) (car ,dr)))
       ((not (and ,v
		  (= ,d (dzien ,v))
		  (= ,h (godzina ,v)))))
     (prog1 ,A
       (setf ,dr (cdr ,dr)))))
	
(defmacro if-dyzur ((dr d h) A B)
  `(if (and ,dr
	    (= ,d (dzien (car ,dr)))
	    (= ,h (godzina (car ,dr))))
       (progn ,A)
       (progn ,B)))
