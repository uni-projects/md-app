
(ql:quickload '(:swank :cl-who :hunchentoot :parenscript :elephant :md5 :cl-ppcre-unicode))

(defpackage :md-app
  (:use :cl :cl-who :hunchentoot :parenscript :elephant))
(in-package :md-app)

;; domyślne porty
(defparameter *httpd-port* 8080)
(defparameter *shutdown-port* 6200)
(defparameter *swank-port* 4005)

;; konfiguracja bazy danych
(defparameter *db-config*
  '(:clsql (:postgresql 
	    "localhost" "md-app" "md-app" "md-app-password")))

;; ścieżki dostępu
(defparameter *path* 
  (make-pathname :directory '(:absolute "home" "jack" "projekt" "final")
		 :type "lisp"))
(defparameter *httpd-/* "/home/jack/public_html/")
(defparameter *httpd-error* "/home/jack/public_html/errors/")

;; baza danych i definicje metaklas
(defparameter *store*
  (open-store *db-config*))
(load (merge-pathnames *path* "user"))
(load (merge-pathnames *path* "user-osoba"))
(load (merge-pathnames *path* "user-pacjent"))
(load (merge-pathnames *path* "user-lekarz"))
(load (merge-pathnames *path* "user-poradnia"))
(load (merge-pathnames *path* "user-admin"))

;; domain specific language
(load (merge-pathnames *path* "dsl-standard-page.lisp"))
(load (merge-pathnames *path* "dsl-standard-form.lisp"))
(load (merge-pathnames *path* "dsl-standard-listing.lisp"))
(load (merge-pathnames *path* "dsl-standard-plan.lisp"))

;; serwer www
(defparameter *httpd*
  (hunchentoot:start
   (make-instance 'hunchentoot:easy-acceptor
		  :port *httpd-port*
		  :document-root *httpd-/*
		  :error-template-directory *httpd-error*)))

(load (merge-pathnames *path* "page-home.lisp"))
(load (merge-pathnames *path* "page-login.lisp"))
(load (merge-pathnames *path* "page-register.lisp"))
(load (merge-pathnames *path* "page-listing.lisp"))
(load (merge-pathnames *path* "page-modify.lisp"))
(load (merge-pathnames *path* "page-wizyty.lisp"))

;; serwer lispa
(defparameter *swank-server*
  (swank:create-server :port *swank-port*
		       :dont-close t))


