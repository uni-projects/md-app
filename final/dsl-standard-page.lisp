(in-package :md-app)
(defparameter *greetings* (make-hash-table :test #'eq))

(defmacro standard-page ((&key title privigiled) &body body)
    "Makro zawiera szablon strony"
  `(start-session)
  `(let ((status (session-value :status)))
     (unless 
  	 (or 
  	  (find (class-name (class-of status)) ,privigiled)
  	  (not ,privigiled))
       (handle-static-file "errors/404.html"))
     (with-html-output-to-string 
	 (*standard-output* nil :prologue t :indent t)
       (:html
	(:head (:meta :charset "utf-8")
	       (:title ,title)
	       (:link :type "text/css"
		      :rel "stylesheet"
		      :href "/md-app.css")
	       (:script :type "text/javascript"
			:src "/scripts/user.js")
	       (:script :type "text/javascript"
			:src "/scripts/forms.js"))
		      
	(:body
	 (:div :id "header" ; logo, cokolwiek
	       (fmt "~a" (gethash 
			  (class-of status) 
			  *greetings*)))
	 (:hr)
	,@body
	(:hr)
	(:div :id "footer" ; stopka
	      (:img :src "/img/made-with-lisp-logo.jpg"
		    :width 300 :height 100)))))))

(let ((n (class-of nil))
      (p (find-class 'pacjent))
      (l (find-class 'lekarz))
      (a (find-class 'admin)))
  (setf (gethash n *greetings*)
	(with-html-output (*standard-output* nil :indent t)
	 (:div :id "greet" "Jesteś zalogowany jako " (:i "Gość."))
	 (:div :id "nav"
	       (:a :href "/" "Start")
	       (:a :href "zaloguj" "Zaloguj")
	       (:a :href "rejestracja" "Zarejestruj"))))
  (setf (gethash p *greetings*)
	(with-html-output-to-string (*standard-output* nil :indent t)
	 (:span :class "greet" "Jesteś zalogowany jako " (:i "Pacjent."))
	 (:div :id "nav"
	       (:a :href "wizyty" "Twoje wizyty")
	       (:a :href "umow" "Umów wizytę")
	       (:a :href "ustawienia" "Ustawienia")
	       (:a :href "wyloguj" "Wyloguj"))))
  (setf (gethash l *greetings*)
	(with-html-output-to-string (*standard-output* nil :indent t)
	 (:span :class "greet" "Jesteś zalogowany jako " (:i "Lekarz."))
	 (:div :id "nav"
	       (:a :href "/wizyty" "Lista wizyt")
	       (:a :href "/rozklad" "Plan pracy")
	       (:a :href "/ustawienia" "Ustawienia")
	       (:a :href "/wyloguj" "Wyloguj"))))
  (setf (gethash a *greetings*)
	(with-html-output-to-string (*standard-output* nil :indent t)
	 (:span :class "greet" "Jesteś zalogowany jako " (:i "Administrator."))
	 (:div :id "nav"
	       (:a :href "admin-panel" "Panel administratora")
	       (:a :href "wyloguj" "Wyloguj")))))
