(in-package :md-app)

(define-easy-handler (zmien :uri "/ustawienia" :default-request-type :POST) 
    (imie nazwisko nazwa)
  (standard-page (:title "Zmodyfikuj ustawienia")
    (:h1 "Zmiana ustawień")
    (if (or nazwa imie nazwisko)
	(progn
	  (if imie
	      (setf (imie status)
		    imie))
	  (if nazwisko 
	      (setf (nazwisko status)
		    nazwisko))
	  (if nazwa
	      (setf (nazwa status)
		    nazwa))
	  (htm (:p "Modyfikacja zakończona pomyślnie."))))
    
    (if (eq (class-of status) (find-class 'poradnia))
	(standard-form ("Modyfikuj ustawienia" "ustawienia")
	  (:label "Nazwa" :type :text :name "nazwa")
	  (:label "Zatwierdź zmiany" :type :submit))
	(standard-form ("Modyfikuj ustawienia" "ustawienia")
	  (:label "Imię" :type :text :name "imie")
	  (:label "Nazwisko" :type :text :name "nazwisko")
	  (:label "Zatwierdź zmiany" :type :submit)))))

