(in-package :md-app)

(define-easy-handler (zaloguj :uri "/zaloguj")
    ((login :request-type :POST)
     (haslo :request-type :POST))
  (let ((uzytkownik (znajdz-uzytkownika login)))
    (standard-page (:title "Logowanie")
      (:h1 "Logowanie")
      (cond
	((session-value :status)
	 (htm (:p "Jesteś już zalogowany.")))
	((not (and login haslo))
	 (standard-form ("Formularz logowania" "zaloguj"
	   '(("Login" "login" "text")
	     ("Hasło" "haslo" "password")))
	   (:tr
	    (:td :colspan 2
		 (:input :type "submit"
			 :value "Zaloguj się")))))
	((not uzytkownik)
	 (htm (:p :class "error" "Podane konto nie istnieje.")
	      (:a :href "zaloguj" "Spróbuj ponownie.")))
	((not (aktywny uzytkownik))
	 (htm (:p :class "error" "Konto użytkownika nie jest jeszcze aktywne.")
	      (:a :href "zaloguj" "Spróbuj ponownie.")))
	((not (zaloguj-uzytkownika login haslo))
	 (htm (:p :class "error" "Podane hasło jest nieprawidłowe.")
	      (:a :href "zaloguj" "Spróbuj ponownie.")))
	(T
	 (htm (:p "Zostałeś zalogowany pomyślnie."))
	 (redirect "/"))))))
	


(define-easy-handler (wyloguj :uri "/wyloguj") ()
  (wyloguj-uzytkownika)
  (standard-page (:title "Wylogowanie")
    (:h1 "Wylogowanie")
    (:p "Zostałeś wylogowany z serwisu.")
    (redirect "/")))
