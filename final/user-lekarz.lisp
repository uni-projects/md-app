(in-package :md-app)
(defclass lekarz (osoba)
  ((pwz :accessor pwz
	  :initarg :pwz))
  (:metaclass persistent-metaclass))

(let ((scanner (cl-ppcre:create-scanner "^[1-9][0-9]{6}$")))
  (defun sprawdz-lekarza (pwz)
    (let ((acc nil))
      (if (not (cl-ppcre:scan scanner pwz))
	  (push "pwz jest niepoprawny" acc)
	  (push nil acc)))))

(defun dodaj-lekarza (login haslo imie nazwisko pwz)
  (let ((res
	 (append
	  (reverse (sprawdz-uzytkownika login haslo))
	  (reverse (sprawdz-osobe imie nazwisko))
	  (reverse (sprawdz-lekarza pwz)))))
    (if (find-if-not #'null res)
	res
	(make-instance 'lekarz
		       :login login
		       :haslo (md5:md5sum-string haslo)
		       :imie imie
		       :nazwisko nazwisko
		       :pwz pwz))))
