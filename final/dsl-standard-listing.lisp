
(defmacro person-to-td (x)
  `(htm
    (:td (fmt "~a" (imie ,x)))
    (:td (fmt "~a" (nazwisko ,x)))
    (user-to-td ,x)))

(defmacro user-to-td (x)
  `(htm
    (:td (fmt "~a" (login ,x)))
    (if (aktywny ,x)
	(htm (:td "Aktywny"))
	(htm (:td "Nieaktywny")))
    (:td
     (if (aktywny ,x)
	 (htm
	  (:img :onclick (format nil "lockUser(\"~a\")" (login x))
		:alt "zablokuj"
		:src "img/block.png"))
	 (htm
	  (:img :onclick (format nil "unlockUser(\"~a\")" (login x))
		:alt "odblokuj"
		:src "img/unlock.png")))
     (:img
      :onclick (format nil "deleteUser(\"~a\")" (login x))
      :alt "usun"
      :src "img/delete.png"))))
    

(defmacro do-lst ((x lst &optional (res nil)) &body body)
  `(dolist (,x 
	     (remove-duplicates
	      (get-instances-by-class ,lst))
	    ,res)
     ,@body))

(defmacro standard-listing (nazwy &body body)
  `(htm
    (:table 
     :id "listing"
     (:tr
      (dolist (n ,nazwy)
	(htm
	 (:th (fmt "~a" n)))))
     ,@body)))
   
