(in-package :md-app)

(define-easy-handler (index :uri "/") ()
  "Główna strona portalu"
  (standard-page (:title "Strona główna")
    (:h1 "Witaj na stronie serwisu MD-APP")
    (:p "Wybierz z górnego menu interesującą Cię opcję.")
    (if (null status)
	(htm (:p "Żeby umówić się na wizytę, musisz być"
		 (:a :href "/zaloguj" "zalogowany.")
		 (:br) "Nie masz konta?"
		 (:a :href "/rejestracja" "Zarejestruj się.")))
	(cond
	  ((eq (class-of status) (find-class 'admin))
	   (redirect "/admin-panel"))
	  ((eq (class-of status) (find-class 'pacjent))
	   (redirect "/wizyty"))
	  ((eq (class-of status) (find-class 'lekarz))
	   (redirect "/wizyty"))))))
