(in-package :md-app)

(defmacro standard-form ((label dest inputs) &body body)
  `(htm
    (:label ,label)
    (:form :action ,dest :method "post"
	   (:table
	    (dolist (tup ,inputs)
	      (destructuring-bind (label name type) tup
		(htm
		 (:tr
		  (:td (fmt "~a" label))
		  (:td (:input :name name
			       :type type))))))
	    ,@body))))

(defmacro combobox ((name values))
  `(htm
    (:select 
     :name ,name
     (dolist (el ,values)
       (destructuring-bind (val opt) el
       (htm (:option 
	     :value val
	     (fmt "~a" opt))))))))
