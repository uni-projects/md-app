(in-package :md-app)

(define-easy-handler (rejestracja-pacjenta :uri "/rejestracja" :default-request-type :POST) 
    (imie nazwisko pesel login haslo)
  (standard-page (:title "Zarejestruj się" :privigiled '(null))
    (:h1 "Rejestracja")
    (if (and imie nazwisko pesel login haslo)
	(let ((res (dodaj-pacjenta login haslo imie nazwisko pesel)))
	  (if (eq (class-of res) (find-class 'pacjent))
	      (htm (:p "Rejestracja zakończona pomyślnie.")
		   (:a :href "zaloguj" "Możesz się zalogować."))
	      (htm (:p :class "error" "Rejestracja nie powiodła się:"
		   (dolist (var res)
		     (if var
			 (htm (:li :class "error" (fmt "~a" var))))))
		   (:a :href "rejestracja" "Spróbuj ponownie."))))

	(standard-form ("Formularz rejestracyjny"
			"rejestracja"
			'(("Imię" "imie" "text")
			  ("Nazwisko" "nazwisko" "text")
			  ("Pesel" "pesel" "text")
			  ("Login" "login" "text")
			  ("Hasło" "haslo" "password")))
	  (:tr
	   (:td :colspan 2
		(:input :type "submit"
			:value "Zarejestruj się")))))))

(define-easy-handler (rejestracja-lekarza :uri "/dodaj-lekarza" :default-request-type :POST) 
    (imie nazwisko pwz login haslo)
  (standard-page (:title "Dodaj lekarza")
    (:h1 "Rejestracja")
    (if (and imie nazwisko pwz login haslo)
	(let ((res (dodaj-lekarza login haslo imie nazwisko pwz)))
	  (if (eq (class-of res) (find-class 'lekarz))
	      (htm (:p "Rejestracja zakończona pomyślnie.")
		   (redirect "/admin-panel"))
	      (htm (:p :class "error" "Rejestracja nie powiodła się:"
		   (dolist (var res)
		     (if var
			 (htm (:li :class "error" (fmt "~a" var))))))
		   (:a :href "dodaj-lekarza" "Spróbuj ponownie."))))
	(standard-form 
	    ("Formularz dodania lekarza"
	     "dodaj-lekarza"
	     '(("Imię" "imie" "text")
	       ("Nazwisko" "nazwisko" "text")
	       ("PWZ" "pwz" "text")
	       ("Login" "login" "text")
	       ("Hasło" "haslo" "password")))
	  (:tr
	   (:td :colspan 2
		(:input :type "submit"
			:value "Dodaj lekarza")))))))

(define-easy-handler (rejestracja-poradni :uri "/dodaj-poradnie" :default-request-type :POST) 
    (nazwa login haslo)
  (standard-page (:title "Dodaj poradnie")
    (:h1 "Rejestracja")
    (if (and nazwa login haslo)
	(let ((res (dodaj-poradnie login haslo nazwa)))
	  (if (eq (class-of res) (find-class 'poradnia))
	      (htm (:p "Rejestracja zakończona pomyślnie.")
		   (redirect "/admin-panel"))
	      (htm (:p :class "error" "Rejestracja nie powiodła się:"
		       (dolist (var res)
			 (if var
			     (htm (:li :class "error" (fmt "~a" var))))))
		   (:a :href "dodaj-lekarza" "Spróbuj ponownie."))))
	(standard-form 
	    ("Formularz dodania poradni"
	     "dodaj-poradnie"
	     '(("Nazwa" "nazwa" "text")
	       ("Login" "login" "text")
	       ("Hasło" "haslo" "password")))
	  (:tr
	   (:td :colspan 2
		(:input :type "submit"
			:value "Dodaj poradnię")))))))
