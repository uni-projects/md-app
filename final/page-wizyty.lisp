(in-package :md-app)

(define-easy-handler (rozklad :uri "/rozklad") ()
  (standard-page (:title "Twój plan pracy" :privigiled '(lekarz))
    (let* ((dr (sorted-dyzur (dyzury status)))
	   (funkcja 
	    (lambda (day hour)
	      (if-dyzur (dr day hour)
			(prog1
			    (htm (:td :class "zajete" 
				      :onclick (format nil "delCell(~a, ~a)" day hour)
				      (fmt (nazwa (przychodnia (car dr))))))
			  (setf dr (cdr dr)))
			(prog1 
			    (htm (:td :class "wolne" 
				      :onclick (format nil "addCell(~a, ~a)" day hour)
				      (:img :class "t_op" :src "img/add.png"))))))))
      (standard-plan (funkcja)))))

(define-easy-handler (add :uri "/add")
    ((dzien-tygodnia :request-type :GET)
     (godzina :request-type :GET))
  (let ((hour (or (parse-integer (string godzina) :junk-allowed 0) 0))
	(day (or (parse-integer (string dzien-tygodnia) :junk-allowed 0) 0))
	(usr (get-instances-by-class 'przychodnia)))
    (standard-page (:title "test" :privigiled '(lekarz))
      (standard-form ("Ustal godziny pracy" "/add-plan" nil)
	(:label "Przychodnia" :type :combobox :name "przychodnia"
		:elements 

	(:tr (:td "Przychodnia")
	     (:td (combobox ("przychodnia"
			     (mapcar (lambda (x) (nazwa (dane x))) usr)
			     (mapcar 'ident usr)))))
	(:tr (:td "Dzień tygodnia")
	     (:td (combobox ("dzien-tygodnia" 
			     *dni-tygodnia* 
			     (loop for i from 0 to 6 collecting i)
			     day))))
	(:tr (:td :style "vertical-align: top" "Czas pracy")
	     (:td  (combobox ("start" 
			      *godziny* 
			      (loop for i from 0 to (length *godziny*) collecting i)
			      hour))
		   (combobox ("stop" 
			      *godziny* 
			      (loop for i from 0 to (length *godziny*) collecting i)
			      (1+ hour) ))))))))


(define-easy-handler (del-plan :uri "/del-plan")
    ((dzien-tygodnia :request-type :GET)
     (godzina :request-type :GET))
  (if (not (and dzien-tygodnia godzina)) (redirect "/errors/op"))
  (standard-page (:title "hiho" :privigiled '(lekarz))
    (let ((day (parse-integer dzien-tygodnia))
	  (beg (parse-integer godzina)))
      (drop-instances (dyzury status day beg)))
    (redirect "/rozklad")))

(define-easy-handler (add-plan :uri "/add-plan")
    ((przychodnia :request-type :POST)
     (dzien-tygodnia :request-type :POST)
     (start :request-type :POST)
     (stop :request-type :POST))
  (if (not (and przychodnia dzien-tygodnia start stop)) (redirect "/errors/op"))
  (standard-page (:title "hiho" :privigiled (list 'lekarz))
    (let ((beg (parse-integer start))
	  (end (parse-integer stop))
	  (day (parse-integer dzien-tygodnia)))
      (loop for i from beg to (1- end) do
	   (if (dyzury status day i)
	       (redirect "/errors/op")
	       (make-instance 'dyzur
			      :lekarz status
			      :przychodnia przychodnia
			      :dzien day
			      :godzina i)))
    (redirect "/rozklad")))))

(define-easy-handler (errors :uri "/errors/op") ()
  (standard-page (:title "Operacja nie powiodła się")
    (:p "Operacja nie powiodła się.")))

(define-easy-handler (java-scripts :uri "/scripts/forms.js") ()
  (setf (content-type*) "text/javascript")
  (ps (defun ask-cell (day hour loid)
	(setf document.location.href (+ "/ask?dzien=" day "&godzina=" hour "&lekarz=" loid)))
      (defun del-cell (day hour)
	(setf accepted (confirm "Czy na pewno chcesz usunąć ten blok?"))
	(if accepted
	    (setf document.location.href (+ "/del-plan?dzien-tygodnia=" day "&godzina=" hour))))
      (defun add-cell (day hour)
	(setf document.location.href (+ "/add?dzien-tygodnia=" day "&godzina=" hour)))))
;	(setf document.location.href (+ "/sub?utc=" utc "&prz=" prz "&lek=" lek)))))

