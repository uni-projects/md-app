(in-package :md-app)

(defclass poradnia (uzytkownik)
  ((nazwa :accessor nazwa
	  :initarg :nazwa))
  (:metaclass persistent-metaclass))

(defun sprawdz-poradnie (nazwa)
  (let ((app nil))
    (if (not nazwa)
	(push "Musisz podać nazwę poradni" app)
	(push nil app))))

(defun dodaj-poradnie (login haslo nazwa)
  (let ((res
	 (append
	  (reverse (sprawdz-uzytkownika login haslo))
	  (reverse (sprawdz-poradnie nazwa)))))
    (if (find-if-not #'null res)
	res
	(make-instance 'poradnia
		       :login login
		       :haslo haslo
		       :nazwa nazwa))))
