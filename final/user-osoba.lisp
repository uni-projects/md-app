(in-package :md-app)
(defclass osoba (uzytkownik)
  ((imie :accessor imie
	 :initarg :imie)
   (nazwisko :accessor nazwisko
	     :initarg :nazwisko))
  (:metaclass persistent-metaclass))

(let ((scanner (cl-ppcre:create-scanner "^\\p{Lu}\\p{Ll}*$")))
  (defun sprawdz-osobe (imie nazwisko)
    (let ((acc nil))
      (if (not (cl-ppcre:scan scanner imie))
	  (push "imię jest niepoprawne" acc)
	  (push nil acc))
      (if (not (cl-ppcre:scan scanner nazwisko))
	  (push "nazwisko jest niepoprawne" acc)
	  (push nil acc)))))
