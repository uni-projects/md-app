(in-package :md-app)
(defclass pacjent (osoba)
  ((pesel :accessor pesel
	  :initarg :pesel))
  (:metaclass persistent-metaclass))

(let ((scanner (cl-ppcre:create-scanner "^[0-9]{11}$")))
  (defun sprawdz-pacjenta (pesel)
    (let ((acc nil))
      (if (not (cl-ppcre:scan scanner pesel))
	  (push "pesel jest niepoprawny" acc)
	  (push nil acc)))))

(defun dodaj-pacjenta (login haslo imie nazwisko pesel)
  (let ((res
	 (append
	  (reverse (sprawdz-uzytkownika login haslo))
	  (reverse (sprawdz-osobe imie nazwisko))
	  (reverse (sprawdz-pacjenta pesel)))))
    (if (find-if-not #'null res)
	res
	(make-instance 'pacjent
		       :login login
		       :haslo (md5:md5sum-string haslo)
		       :imie imie
		       :nazwisko nazwisko
		       :pesel pesel))))
