(in-package :md-app)
(defpclass uzytkownik ()
  ((ident :reader ident
	  :initform (elephant-data-store:next-oid *store*)
	  :index t)
   (login :reader login
	  :initarg :login
	  :index t)
   (haslo :reader haslo
	  :initarg :haslo)
   (aktywny :accessor aktywny
	    :initform nil
	    :index t)))

(let ((login-scanner (cl-ppcre:create-scanner "^[a-zA-Z_][a-zA-Z0-9_]{2,15}$"))
      (passw-scanner (cl-ppcre:create-scanner ".{5,}")))
  (defun sprawdz-uzytkownika (login haslo)
    (let ((acc nil))
	(if (znajdz-uzytkownika login)
	    (push "użytkownik istnieje" acc)
	    (push nil acc))
	(if (not (cl-ppcre:scan login-scanner login))
	    (push "login jest niepoprawny (min. 3 znaki, pierwsza litera)" acc)
	    (push nil acc))
	(if (not (cl-ppcre:scan passw-scanner haslo))
	    (push "hasło jest niepoprawne (min. 5 znaków)" acc)
	    (push nil acc)))))

(defun znajdz-uzytkownika (login)	; sugar
  ;; (with-open-store (*db-config*)
    (or (get-instance-by-value 'pacjent 'login login)
	(get-instance-by-value 'lekarz 'login login)
	(get-instance-by-value 'poradnia 'login login)
	(get-instance-by-value 'admin 'login login)));)

(defun usun-uzytkownika (login)
  (let ((uzytkownik (znajdz-uzytkownika login)))
    (and uzytkownik
	 (drop-instance (znajdz-uzytkownika login)))))

(defun zaloguj-uzytkownika (login haslo)
  (if (not (session-value :status))
      (let ((uzytkownik (znajdz-uzytkownika login)))
	(and uzytkownik
	     (aktywny uzytkownik)
	     (equalp (md5:md5sum-string haslo)
		     (haslo uzytkownik))
	     (setf (session-value :status) uzytkownik)))))

(defun wyloguj-uzytkownika ()
  (if (session-value :status)
      (delete-session-value :status)))

(defun aktywuj-uzytkownika (login &optional (stan T))
  (setf (aktywny (znajdz-uzytkownika login))
	stan))
      
